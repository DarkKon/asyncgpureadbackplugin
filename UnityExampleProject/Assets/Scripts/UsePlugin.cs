using UnityEngine;
using System.Collections.Generic;
using System.IO;
using AsyncGPUReadbackPluginNs;

/// <summary>
/// Exemple of usage inspirated from https://github.com/keijiro/AsyncCaptureTest/blob/master/Assets/AsyncCapture.cs
/// </summary>
public class UsePlugin : MonoBehaviour {
    Queue<AsyncGPUReadbackRequest> _requests = new Queue<AsyncGPUReadbackRequest>();

    void Update()
    {
        while (_requests.Count > 0)
        {
            var req = _requests.Peek();

            if (req.hasError)
            {
                Debug.Log("GPU readback error detected.");
                _requests.Dequeue();
            }
            else if (req.done)
            {
                var buffer = req.GetData<byte>().ToArray();
                var camera = GetComponent<Camera>();
                SaveBitmap(buffer, camera.pixelWidth, camera.pixelHeight);

                _requests.Dequeue();
            }
            else
            {
                break;
            }
        }
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination);

        if (Time.frameCount % 60 == 0)
        {    
            if (_requests.Count < 8)
                _requests.Enqueue(AsyncGPUReadback.Request(source));
            else
                Debug.LogWarning("Too many requests.");
        }
    }

    void SaveBitmap(byte[] buffer, int width, int height)
    {
        Debug.Log("Write to file");
        var tex = new Texture2D(width, height, TextureFormat.RGBAHalf, false);
        tex.LoadRawTextureData(buffer);
        tex.Apply();
        File.WriteAllBytes("test.png", ImageConversion.EncodeToPNG(tex));
        Destroy(tex);
    }
}
